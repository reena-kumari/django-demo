from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    #return HttpResponse('Hello')
    return render(request, 'home.html',{'some':"value"})

def cake(request):
    return HttpResponse('<h1>My cake</h1>')    

def count(request): 
    fulltext = request.GET['fulltext']   
    worldlist = fulltext.split()
    wordsDic = countByWords(worldlist)
    return render(request, 'count.html',{
        'value':fulltext,
        'count':len(worldlist),
        'worddic':wordsDic
        })

def countByWords(worldlist):
    wordsDic ={}
    for word in worldlist:
        if word in wordsDic:
            wordsDic[word]+=1
        else:
            wordsDic[word] = 1
    return wordsDic