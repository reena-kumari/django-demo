#class name cap satrts
#normal { is : in python
#class can hv function and variable
class Dog:
    #class variable, same as static in other lang
    dogInfo = "hey dog"
    
    #instance variable
    def __init__(self, name):
        self.name = name
    def bark(self): #sould be self (similar to no arg)
        print("BARK")

mydog = Dog("ho")
mydog.bark()
#attach instance variable anytime to object
mydog.age =10

print(mydog.name)

print(Dog.dogInfo)
