#ordered and immutable
greeting = ("Hello","Hi","Good morning","Hi", 1)
print(greeting)

for x in greeting:
  print(x)

#single value tuple
a = ("AA",) #tuple
print(type(a))
a = ("AA") #string
print(type(a))

print(len(greeting))

print(greeting[1])
#range
print(greeting[1:4]) #including 1st index and excluding 4th
print(greeting[1:])#starts from 1st till end
print(greeting[:4])#from bigning till 3rd index

#-ve value index, -1 means last value
print(greeting[-4:-1]) #last -4 included, -1 excluded

#check item in tuple
if "Hello" in greeting:
    print("yes")

#Tuple is immutable, so can't change value after creating
# can be achived by converting to list and then change
# and convert back to tuple
newgreetings = list(greeting)
print(newgreetings)

newgreetings[1] = "Good night"
greeting = tuple(newgreetings)
print(greeting)

#Packing-Unpacking 
#assign value to tuple - packing
#extract values from tuple - unpacking

department = ("Sale","HR","Manager") #packing
(lower, higher, reporter) = department #unpacking

print(lower)
print(higher)
print(reporter)

# * used to assign remaning values as list
(reportee, *reporter) = department
print(reportee)
print(reporter)

(*reportee, reporter) = department
print(reportee)
print(reporter)
