
#no arg
def hello():
    print("hello world")

hello()
hello() 

#parameter
def some(str):
    print(str)

some(12)
some("so")

#default value
#hello function replaced the above hello function, 
#no error , new hello will be called
#return type no need to specify
def hello(name="so", age=20):
    print("hello {} age {}".format(name, age))
    return "go"

print(hello()) 

#lambda is a small anonymous function
#it takes any number of arguments 
#can hv only one expression, which returns
x = lambda a:a+10 
print(x(2))