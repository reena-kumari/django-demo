#JS object, 
#key-value

dog = {
    "some":23,
    "heo":10
}
print(dog)
print(dog["heo"])
dog[12] ="hhh"

print(dog) #{'some': 23, 'heo': 10, 12: 'hhh'}
#converting dic to list
#each item in list holds key value pairs
print(dog.items()) #dict_items([('some', 23), ('heo', 10), (12, 'hhh')])